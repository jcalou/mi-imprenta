    <div class="container">
        <div class="card card-container">
            <img id="profile-img" class="profile-img-card" src="<?=base_url('assets/images');?>/logo.png" />
            <p id="profile-name" class="profile-name-card"></p>
            <form action="<?=base_url('user/nuevo');?>" method="post" class="form-signin">
                <span id="reauth-email" class="reauth-email"></span>
                <input type="text" id="inputUsername" name="username" class="form-control" placeholder="username" required autofocus>
                <input type="email" id="inputEmail" name="email" class="form-control" placeholder="email" >
                <input type="password" id="inputPassword" name="password" class="form-control" placeholder="password" required>
                <input type="password" id="inputPassword2" name="password2" class="form-control" placeholder="repeat password" required>
                <input type="text" id="inputFullname" name="fullname" class="form-control" placeholder="nombre completo" >
                <input type="hidden" name="post" value="1">
                <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Crear cuenta</button>
            </form><!-- /form -->
            <a href="<?=base_url('user/login');?>" class="create-account">
                Ya tengo cuenta
            </a>
        </div><!-- /card-container -->
    </div><!-- /container -->