	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<table class="table table-striped table-bordered table-hover table-condensed">
					<thead>
						<tr>
							<th>#</th>
							<th>Fecha</th>
							<th>Cliente</th>
							<th>Estado</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>5</td>
							<td>10/05/2015</td>
							<td>Juan Calou</td>
							<td>Pendiente</td>
							<td>
								<!-- Extra small button group -->
								<div class="btn-group">
								  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
								    Acciones <span class="caret"></span>
								  </button>
								  <ul class="dropdown-menu" role="menu">
								    <li><a href="#">Ver detalle</a></li>
								    <li><a href="#">Procesar</a></li>
								    <li><a href="#">Modificar</a></li>
								    <li class="divider"></li>
								    <li><a href="#">Eliminar</a></li>
								  </ul>
								</div>
							</td>
						</tr>
						<tr class="info">
							<td>2</td>
							<td>09/05/2015</td>
							<td>Matias Bottero</td>
							<td>Procesando</td>
							<td>
								<!-- Extra small button group -->
								<div class="btn-group">
								  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
								    Acciones <span class="caret"></span>
								  </button>
								  <ul class="dropdown-menu" role="menu">
								    <li><a href="#">Ver detalle</a></li>
								    <li><a href="#">Finalizar</a></li>
								    <li><a href="#">Modificar</a></li>
								    <li class="divider"></li>
								    <li><a href="#">Eliminar</a></li>
								  </ul>
								</div>
							</td>
						</tr>
						<tr class="success">
							<td>3</td>
							<td>09/05/2015</td>
							<td>Matias Bottero</td>
							<td>Finalizado</td>
							<td>
								<!-- Extra small button group -->
								<div class="btn-group">
								  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
								    Acciones <span class="caret"></span>
								  </button>
								  <ul class="dropdown-menu" role="menu">
								    <li><a href="#">Ver detalle</a></li>
								    <li class="divider"></li>
								    <li><a href="#">Eliminar</a></li>
								  </ul>
								</div>
							</td>
						</tr>
						<tr class="success">
							<td>4</td>
							<td>09/05/2015</td>
							<td>Matias Bottero</td>
							<td>Finalizado</td>
							<td>
								<!-- Extra small button group -->
								<div class="btn-group">
								  <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
								    Acciones <span class="caret"></span>
								  </button>
								  <ul class="dropdown-menu" role="menu">
								    <li><a href="#">Ver detalle</a></li>
								    <li class="divider"></li>
								    <li><a href="#">Eliminar</a></li>
								  </ul>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

	</div>
    