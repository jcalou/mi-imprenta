
    <div class="container">
        <div class="card card-container">
            <img id="profile-img" class="profile-img-card" src="<?=base_url('assets/images');?>/logo.png" />
            <p id="profile-name" class="profile-name-card"></p>
            <form action="<?=base_url('user/login_check');?>" method="post" class="form-signin">
                <span id="reauth-email" class="reauth-email"></span>
                <input type="text" id="inputEmail" name="username" class="form-control" placeholder="username" required autofocus>
                <input type="password" id="inputPassword" name="password" class="form-control" placeholder="password" required>
                <input type="hidden" name="post" value="1">
                <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Login</button>
            </form><!-- /form -->
            <a href="<?=base_url('user/nuevo');?>" class="create-account">
                Crear una cuenta
            </a>
        </div><!-- /card-container -->
    </div><!-- /container -->