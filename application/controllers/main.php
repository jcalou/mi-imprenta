<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Main extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library("auth");
        if( ! $this->auth->logged_in()){
            redirect("user/login");
        }
    }
    public function index(){
        $data['title'] = 'Mi Imprenta';
        $data['description'] = 'Administrador de trabajos de impresion';
        $data['section'] = 'dashboard';
        $data['username'] = $this->session->userdata("user_name");
        if( ! $this->auth->logged_in()){
            redirect("user/login");
        }else{
            if($this->session->userdata("user_level")==1){
                
                $this->load->view('admin_header', $data);
                $this->load->view('admin_dashboard', $data);
                $this->load->view('footer');
            }elseif($this->session->userdata("user_level")>1){
                
                $this->load->view('user_header', $data);
                $this->load->view('user_dashboard', $data);
                $this->load->view('footer');
            }
        }

        
    }

}
