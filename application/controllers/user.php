<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('auth');
        $this->load->library('form_validation');

    }

    public function login(){
        $data['title'] = 'Mi Imprenta';
        $data['description'] = 'Administrador de trabajos de impresion';
        $data['section'] = 'login';
        $data['error'] = "";

        $this->load->view('headerlogin', $data);
        $this->load->view('login', $data);
        $this->load->view('footer');
    }

    public function nuevo(){
        $data['title'] = 'Mi Imprenta';
        $data['description'] = 'Administrador de trabajos de impresion';
        $data['section'] = 'register';
        $data['error'] = "";

        if($this->input->post('post') && $this->input->post('post')==1) {
            $this->form_validation->set_rules('username', 'username', 'required');
            $this->form_validation->set_rules('password', 'password', 'required');

            $data['error'] = "Los dos campos son obligatorios";

            if ($this->form_validation->run() == TRUE) {
                $username = $this->input->post('username');
                $password = $this->input->post('password');
                $email = $this->input->post('email');
                $fullname = $this->input->post('fullname');

                $this->auth->create($username, $password, $email, $fullname);

                if($this->auth->login($username, $password)){
                    redirect('main/index');
                    exit;
                }
            }
        }

        $this->load->view('headerlogin', $data);
        $this->load->view('new_user', $data);
        $this->load->view('footer');
    }

    function login_check() {
        $data['title'] = 'Mi Imprenta';
        $data['description'] = 'Administrador de trabajos de impresion';
        $data['section'] = 'login';
        $data['error'] = "";

        if($this->input->post('post') && $this->input->post('post')==1) {
            $this->form_validation->set_rules('username', 'username', 'required');
            $this->form_validation->set_rules('password', 'password', 'required');

            $data['error'] = "Los dos campos son obligatorios";

            if ($this->form_validation->run() == TRUE) {
                $username = $this->input->post('username');
                $password = $this->input->post('password');

                if($this->auth->login($username, $password)){
                    redirect('main/index');
                    exit;
                }
                $data['error'] =  "Usuario no encontrado";
            }
            $this->load->view('headerlogin', $data);
            $this->load->view('login', $data);
            $this->load->view('footer');
        }
    }

    public function logout(){
        $this->auth->logout();
        redirect('user/login');
    }

}
