<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth {

    function Auth() {
        $CI =& get_instance();
        $CI->load->database();
        $CI->load->library("session");
    }

    function logged_in() {
        $CI =& get_instance();
        return ($CI->session->userdata("user_id")) ? true : false;
    }

    function login($username, $password) {
        $CI =& get_instance();
        $data = array(
            "username" => $username,
            "password" => $password
        );
        $query = $CI->db->get_where("user", $data);
        if($query->num_rows() !== 1) {
            return false;
        } else {
            $CI->session->set_userdata("user_id", $query->row()->id);
            $CI->session->set_userdata("user_name", $query->row()->nombre);
            $CI->session->set_userdata("user_level", $query->row()->nivel);
            return true;
        }
    }

    function create($username, $password, $email, $fullname) {
        $CI =& get_instance();
        $data = array(
            "username" => $username,
            "password" => $password,
            "email" => $email,
            "nivel" => 5,
            "nombre" => $fullname
        );

        $query = $CI->db->insert('user', $data);
        return true;
    }

    function logout() {
        $CI =& get_instance();
        $CI->session->unset_userdata("user_id");
        $CI->session->unset_userdata("user_name");
        $CI->session->unset_userdata("user_level");
    }

}