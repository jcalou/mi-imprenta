<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$route['default_controller'] = "main";
$route['user/(:any)'] = 'user/$1';
$route['(:any)'] = 'main/$1';
$route['404_override'] = '';
